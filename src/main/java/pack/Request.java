package pack;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pack.MySQLJDBCUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

public class Request extends Thread{

    MySQLJDBCUtil mySQLJDBCUtil;
    Connection conn = null;
    Request(String name, MySQLJDBCUtil mySQLJDBCUtil){
        this.setName(name);
        this.mySQLJDBCUtil = mySQLJDBCUtil;
    }
    @Override
    public void run() {
        approach2();
//        System.out.println("hello from thread: "+this.getName());
    }

    public void approach2(){
        ResultSet seatIdRes = null;
        try {
            conn = mySQLJDBCUtil.getConnection();
            conn.setAutoCommit(false);

            String sqlGet = "SELECT * FROM seats where user_id is null order by id limit 1 FOR UPDATE SKIP LOCKED";
            Statement stmt = conn.createStatement();
            seatIdRes = stmt.executeQuery(sqlGet);
            System.out.println("sqlGetQu: "+sqlGet);

            int s1 = -1;
            int s2 = -1;
            while(seatIdRes.next()) {
                if(s1==-1)
                s1 = seatIdRes.getInt(1);
                else s2 = seatIdRes.getInt(1);
            }

            try {
                int userId = Integer.valueOf(this.getName()) +1;
                System.out.println("selectedSeatId: "+s1+" "+s2);
                String sqlQuery = "UPDATE seats " +
                        "SET user_id ="+userId+" WHERE id ="+s1;
                System.out.println("UpdateQuery: "+sqlQuery+" for thread: "+this.getName()+" for user: "+this.getName());
                stmt.executeUpdate(sqlQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            conn.commit();
        } catch (SQLException ex) {
            // roll back the transaction
            try{
                if(conn != null)
                    conn.rollback();
            }catch(SQLException e){
                System.out.println(e.getMessage());
            }
            System.out.println(ex.getMessage());
        } finally {
            try {
                if(seatIdRes != null)  seatIdRes.close();
                if(conn != null) conn.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public void approach1() {

        // for getting  seat id
        ResultSet seatIdRes = null;

        try {
            conn = mySQLJDBCUtil.getConnection();
            conn.setAutoCommit(false);

            int randSeatId = new Random().nextInt(20);
            System.out.println("RandomSeatId: "+randSeatId+" "+this.getName());
            String sqlGet = "SELECT * FROM seats where id="+randSeatId;
            Statement stmt = conn.createStatement();
            seatIdRes = stmt.executeQuery(sqlGet);
            System.out.println("sqlGetQu: "+sqlGet);

            while(seatIdRes.next()) {
                System.out.println(seatIdRes.getInt(1) + " " + seatIdRes.getString(2) + seatIdRes.getInt(3) + " " + seatIdRes.getInt(4) + "  Thread:" + this.getName());
            }

            try {
                int randomUserId = new Random().nextInt(10)+1;
                String sqlQuery = "UPDATE seats " +
                        "SET user_id ="+randomUserId+" WHERE id="+randSeatId;
                System.out.println("UpdateQuery: "+sqlQuery);
                stmt.executeUpdate(sqlQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            conn.commit();
        } catch (SQLException ex) {
            // roll back the transaction
            try{
                if(conn != null)
                    conn.rollback();
            }catch(SQLException e){
                System.out.println(e.getMessage());
            }
            System.out.println(ex.getMessage());
        } finally {
            try {
                if(seatIdRes != null)  seatIdRes.close();
                if(conn != null) conn.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}

