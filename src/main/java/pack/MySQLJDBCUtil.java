package pack;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.Arrays;
import java.util.Random;

@Component
@PropertySource(value= {"application.properties"})
public class MySQLJDBCUtil {

    private String dbUrl;
    private String dbUser;
    private String dbPass;
    @Autowired
    public MySQLJDBCUtil(@Value("${spring.datasource.url}") String dbUrl , @Value("${spring.datasource.username}") String dbUser , @Value("${spring.datasource.password}") String dbPass) {

        this.dbUrl = dbUrl;
        this.dbUser = dbUser;
        this.dbPass = dbPass;
    }

    @Override
    public String toString() {
        return "MySQLJDBCUtil{" +
                "dbUrl='" + dbUrl + '\'' +
                ", dbUser='" + dbUser + '\'' +
                ", dbPass='" + dbPass + '\'' +
                '}';
    }


    Connection getConnection(){
        try {
//            System.out.println("url: "+this.dbUrl);
            return DriverManager.getConnection(
                    dbUrl,dbUser,dbPass);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    void showDataInTable(){
        Connection conn = null;
        int x[]= new int[20];
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            try {
                String sqlGetAll = "SELECT * FROM seats";
                ResultSet rs = stmt.executeQuery(sqlGetAll);
                while(rs.next()){
                    int val = rs.getInt(4);
                    int id = rs.getInt(1);
                    x[id-1] = val;
                }
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            conn.commit();
        } catch (SQLException ex) {
            // roll back the transaction
            try{
                if(conn != null)
                    conn.rollback();
            }catch(SQLException e){
                System.out.println(e.getMessage());
            }
            System.out.println(ex.getMessage());
        } finally {
            try {
                if(conn != null) conn.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

        System.out.println(Arrays.toString(x));
        for(int i=0;i<20;i++){
            if(i>=5 && i%5==0)
                System.out.print("\t");
            if(i>=10 && i%10==0)
                System.out.println();
            System.out.print(x[i]);
        }
//        System.out.println("hello!!");
    }

    void clear(){
        Connection conn = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            try {
                String clear = "update seats set user_id = null";
                stmt.executeUpdate(clear);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            conn.commit();
        } catch (SQLException ex) {
            // roll back the transaction
            try{
                if(conn != null)
                    conn.rollback();
            }catch(SQLException e){
                System.out.println(e.getMessage());
            }
            System.out.println(ex.getMessage());
        } finally {
            try {
                if(conn != null) conn.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println("\ncleared table!!");
    }
}
