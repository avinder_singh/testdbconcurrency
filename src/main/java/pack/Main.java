package pack;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

@ComponentScan(basePackages = "pack")
public class Main {

    static int threadCount = 10; // 20 seats, 20 concurrent user requests
    public static void main(String[] args) {
        long oldTimeStamp = Timestamp.from(Instant.now()).getTime();
        System.out.println("start time: "+oldTimeStamp);
        ApplicationContext context = new AnnotationConfigApplicationContext(Main.class);
        MySQLJDBCUtil dbUtil = context.getBean(MySQLJDBCUtil.class);
        Thread threadArr[] = new Thread[threadCount];
        for(int i=0;i<threadCount;i++){
            threadArr[i] = new Request(i+"", dbUtil);
        }
       for(int i=0;i<threadCount;i++)
           threadArr[i].start();

        for(int i=0;i<threadCount;i++) {
            try {
                threadArr[i].join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        long toTimeStamp = Timestamp.from(Instant.now()).getTime();
        System.out.println("end time: "+toTimeStamp);

        System.out.println("Time taken: "+(toTimeStamp-oldTimeStamp));
        dbUtil.showDataInTable();
        dbUtil.clear();
        System.out.println("hello world");
    }


}